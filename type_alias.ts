type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    model: CarModel,
    type: CarType,
    year: CarYear,
}

const caryear: CarYear = 2001;
const carType: CarType = "Toyota";
const carModel: CarModel = "Corolla"

const car1 : Car = {
    year: 2001,
    type: "Nissan",
    model: "xxx"
}
console.log(car1);